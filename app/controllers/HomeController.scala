package controllers

import javax.inject._
import play.api._
import play.api.mvc._

@Singleton
class HomeController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {
def hello() = Action { implicit request: Request[AnyContent] =>
  Ok(views.html.hello())
}
  
}
